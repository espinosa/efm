package org.bitbucket.espinosa.efm.fsutils;

import org.eclipse.e4.core.di.annotations.Creatable;

public interface FileSizeFormatterProvider {
	FileSizeFormatter get();
	
	@Creatable
	public static class InFull implements FileSizeFormatterProvider {
		private final FileSizeFormatter formatter = new FileSizeFormatter.InFull();
	
		@Override
		public FileSizeFormatter get() {
			return formatter;
		}
	}
	
	@Creatable
	public static class InBinaryPrefix implements FileSizeFormatterProvider {
		private final FileSizeFormatter formatter = new FileSizeFormatter.InBinaryPrefix();
	
		@Override
		public FileSizeFormatter get() {
			return formatter;
		}
	}
}
