package org.bitbucket.espinosa.efm.fsutils;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import org.eclipse.e4.core.di.annotations.Creatable;

@Creatable
public class LongDateTimeFormatterProvider implements DateTimeFormatterProvider {
	private DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(
			FormatStyle.MEDIUM, 
			FormatStyle.SHORT);

	@Override
	public DateTimeFormatter get() {
		return formatter;
	}
}
