package org.bitbucket.espinosa.efm.fsutils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.bitbucket.espinosa.efm.exeptions.FsException;


public class FileHandler {

	public static String getFileLastModifiedTimeAsString(Path file, DateTimeFormatter formatter) {
		if (file == null) return "???";
		
		BasicFileAttributes attr;
		try {
			attr = Files.readAttributes(file, BasicFileAttributes.class);
		} catch (IOException e) {
			throw new FsException("Failed on getting file attributes for " + file, e);
		}

		FileTime lastModificationFileTime = attr.lastModifiedTime();
		return LocalDateTime.ofInstant(lastModificationFileTime.toInstant(), ZoneId.systemDefault()).format(formatter);
	}
	
	public static long getFileLastModifiedTimeAsLong(Path file) {
		if (file == null) return 0;
		
		BasicFileAttributes attr;
		try {
			attr = Files.readAttributes(file, BasicFileAttributes.class);
		} catch (IOException e) {
			throw new FsException("Failed on getting file attributes for " + file, e);
		}

		FileTime lastModificationFileTime = attr.lastModifiedTime();
		return lastModificationFileTime.toMillis();
	}
	

	public static long getFileSizeAsLong(java.nio.file.Path file) {
		if (file == null) return 0;
		try {
			return Files.size(file);
		} catch (IOException e) {
			throw new FsException("Failed on getting file size for " + file, e);
		}
	}
	
	public static String getFileSizeAsString(java.nio.file.Path file) {
		if (file == null) return "???";
		try {
			return String.valueOf(Files.size(file));
		} catch (IOException e) {
			throw new FsException("Failed on getting file size for " + file, e);
		}
	}
	
	/**
	 * Simple file suffix extractor.  
	 * Examples:
	 * <pre>
	 * "example.doc" => "doc"
	 * "app.exe"     => "exe"
	 * "bindata"     => ""
	 * ".settings"   => ""
	 * "oddname."    => ""
	 * </pre>
	 * @param path
	 * @return
	 */
	public static String getFileType(Path path) {
		String s = path.getFileName().toString();
		int p = s.lastIndexOf('.');
		String r;
		if (p > 0) {
			r = s.substring(p + 1, s.length());
		} else {
			r = "";
		}
		return r;
	}
}
