package org.bitbucket.espinosa.efm.fsutils;

import java.time.format.DateTimeFormatter;


public interface DateTimeFormatterProvider {
	DateTimeFormatter get();
	
//	@Creatable
//	public static class Compact implements DateTimeFormatterProvider {
//		private DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT, FormatStyle.SHORT);
//
//		@Override
//		public DateTimeFormatter get() {
//			return formatter;
//		}
//	}

}
