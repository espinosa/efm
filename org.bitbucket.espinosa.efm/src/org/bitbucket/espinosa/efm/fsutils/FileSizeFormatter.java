package org.bitbucket.espinosa.efm.fsutils;

/**
 * For display formatter of file sizes.
 * 
 * @author Espinosa
 */
public interface FileSizeFormatter {
	String format(long fileSize);

	/**
	 * Example:  1,454,567 B
	 */
	public static class InFull implements FileSizeFormatter {
		@Override
		public String format(long fileSize) {
			return String.format("%,d B", fileSize);
		}
	}
	
	/**
	 * Example: 25 MiB, 10 KiB or 5 B. 
	 * See: https://en.wikipedia.org/wiki/Binary_prefix
	 */
	public static class InBinaryPrefix implements FileSizeFormatter {
		private static final int unitSize = 1024;
		
		/**
		 * See: http://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java
		 * @param valueInBytes
		 * @return
		 */
		private String convert(long valueInBytes) {
			if (valueInBytes < unitSize) return valueInBytes + " B";
			int highestUnitIndex = (int) (Math.log(valueInBytes) / Math.log(unitSize));
			String highestUnitName = "KMGTPE".charAt(highestUnitIndex - 1) + "iB";
			double valueInHighestUnit = valueInBytes / Math.pow(unitSize, highestUnitIndex);
			return String.format("%.1f %s", valueInHighestUnit, highestUnitName);
		}

		@Override
		public String format(long fileSize) {
			return convert(fileSize);
		}
	}
}
