package org.bitbucket.espinosa.efm.comparators;

import java.nio.file.Path;
import java.util.Comparator;

/**
 * Static factory for creating useful combination of {@link DirectedComparator},
 * {@link CompositeComparator} and {@link SplitCompositeComparator}.
 * 
 * @author Espinosa
 */
public class FileListComparator {
	
	public static Comparator<Path> get(
			Comparator<Path> coreComparator, 
			int direction, // 1 for asc, -1 for desc
			boolean dirFirstFlag
			) {
		if (!dirFirstFlag) {
			return new DirectedComparator(coreComparator, direction);
		} else {
			return new CompositeComparator<Path>(
					CoreComparator.DIRECTORY_FIRST,
					new DirectedComparator(coreComparator, direction));
		}
	}
	
	public static Comparator<Path> get(
			Comparator<Path> coreComparator, 
			int direction, // 1 for asc, -1 for desc
			boolean dirFirstFlag,
			Comparator<Path> dirCoreComparator, 
			int dirDirection // 1 for asc, -1 for desc
			) {
		if (!dirFirstFlag) {
			return new DirectedComparator(coreComparator, direction);
		} else if (direction == dirDirection) {
			return new CompositeComparator<Path>(
					CoreComparator.DIRECTORY_FIRST,
					new DirectedComparator(coreComparator, direction));
		} else {
			return new SplitCompositeComparator<Path>(
					SplitCompositeComparator.DIRECTORY_FIRST,
					new DirectedComparator(dirCoreComparator, dirDirection),
					new DirectedComparator(coreComparator, direction));
		}
	}
}
