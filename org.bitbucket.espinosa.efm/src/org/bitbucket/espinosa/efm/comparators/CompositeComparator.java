package org.bitbucket.espinosa.efm.comparators;

import java.util.Comparator;

/**
 * Composite comparator with support for "shortcuts". 
 * 
 * @author Espinosa
 */
public class CompositeComparator<T> implements Comparator<T> {
	private final Comparator<T>[] comparators;

	@SafeVarargs
	public CompositeComparator(Comparator<T>... comparators) {
		this.comparators = comparators;
	}

	@Override
	public int compare(T o1, T o2) {
		for (Comparator<T> subComparator : comparators) {
			int result = subComparator.compare(o1, o2);
			if (result != 0) {
				return result;
			}
		}
		return 0;
	}
}
