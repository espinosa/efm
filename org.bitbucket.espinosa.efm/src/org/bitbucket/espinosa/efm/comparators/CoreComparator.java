package org.bitbucket.espinosa.efm.comparators;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

import org.bitbucket.espinosa.efm.fsutils.FileHandler;


public class CoreComparator {
	
	public static final Comparator<Path> DIRECTORY_FIRST = (Path p1, Path p2) -> {
		boolean b1 = Files.isDirectory(p1);
		boolean b2 = Files.isDirectory(p2);
		return Boolean.compare(b2, b1);
	};
	
	public static final Comparator<Path> NAME = (Path p1, Path p2) -> {
		return p1.compareTo(p2);
	};
	
	public static final Comparator<Path> DATE = (Path p1, Path p2) -> {
		long d1 = FileHandler.getFileLastModifiedTimeAsLong(p1);
		long d2 = FileHandler.getFileLastModifiedTimeAsLong(p2);
		return Long.compare(d1, d2);
	};
	
	public static final Comparator<Path> SIZE = (Path p1, Path p2) -> {
		if (Files.isDirectory(p1)) {
			return NAME.compare(p1, p2);
		} else {
			long s1 = FileHandler.getFileSizeAsLong(p1);
			long s2 = FileHandler.getFileSizeAsLong(p2);
			return Long.compare(s1, s2);
		}
	};
	
	public static final Comparator<Path> TYPE = (Path p1, Path p2) -> {
		String t1 = FileHandler.getFileType(p1);
		String t2 = FileHandler.getFileType(p2);
		return t1.compareTo(t2);
	};
	
	public static final CompositeComparator<Path> TYPE_NAME = new CompositeComparator<>(TYPE, NAME);
}
