package org.bitbucket.espinosa.efm.comparators;

import java.nio.file.Path;
import java.util.Comparator;

public class DirectedComparator implements Comparator<Path> {
	private final int direction;
	private final Comparator<Path> coreComparator;

	public DirectedComparator(Comparator<Path> coreComparator, int direction) {
		this.coreComparator = coreComparator;
		this.direction = direction;
	}

	@Override
	public int compare(Path o1, Path o2) {
		return coreComparator.compare(o1, o2) * direction;
	}
}