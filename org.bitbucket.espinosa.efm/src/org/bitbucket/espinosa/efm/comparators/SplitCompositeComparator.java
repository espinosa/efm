package org.bitbucket.espinosa.efm.comparators;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.function.Function;

/**
 * Split Composite comparator. Splits sorted items to Priority and Ordinary
 * groups, each with own sorting rules (own comparators). Priority group always
 * precede ordinary groups item.
 * 
 * @author Espinosa
 */
public class SplitCompositeComparator<T> implements Comparator<T> {
	private final Function<T, Boolean> priorityFunction;
	private final Comparator<T> priorityComparator;
	private final Comparator<T> normalComparator;

	public SplitCompositeComparator(
			Function<T, Boolean> priorityFunction,
			Comparator<T> comparator1,
			Comparator<T> comparator2) {
		this.priorityFunction = priorityFunction;
		this.priorityComparator = comparator1;
		this.normalComparator = comparator2;
	}

	@Override
	public int compare(T o1, T o2) {
		boolean b1 = priorityFunction.apply(o1);
		boolean b2 = priorityFunction.apply(o2);
		
		if (b1 && b2) {
			return priorityComparator.compare(o1, o2);
		} else if (!b1 && !b2) {
			return normalComparator.compare(o1, o2);
		} else {
			return Boolean.compare(b2, b1);
		}
	}
	
	/**
	 * Particular example of priority function - directories goes first - identify directory
	 */
	public static final Function<Path, Boolean> DIRECTORY_FIRST = (Path p) -> {
		return Files.isDirectory(p);
	};
}
