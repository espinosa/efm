package org.bitbucket.espinosa.efm.parts;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.stream.Stream;

import org.bitbucket.espinosa.efm.comparators.FileListComparator;
import org.bitbucket.espinosa.efm.comparators.CoreComparator;
import org.bitbucket.espinosa.efm.exeptions.FsException;
import org.bitbucket.espinosa.efm.fsutils.ParentPath;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class DirectoryContentProvider implements IStructuredContentProvider, ITreeContentProvider {

	private Comparator<Path> comparator = FileListComparator.get(CoreComparator.NAME, 1, true);

	public static java.nio.file.Path[] getInitialContent() {
		return null;
	}

	@Override
	public Object[] getElements(Object parentElement) {
		Path parentDir = (Path) parentElement;
		return getElementsAsStream(parentDir).toArray(size -> new java.nio.file.Path[size]);
	}

	private Stream<Path> getElementsAsStream(Path parentDir) {
		if (!Files.isReadable(parentDir)) {
			return Stream.of(); // empty stream
		}
		try {
			Stream<Path> content = Files
					.list(parentDir)
					.sorted(comparator);
			if (parentDir.getParent() == null) { // root path
				return content;
			} else {
				return Stream.concat(Stream.of(new ParentPath(parentDir.getParent())), content);
			}
		} catch (IOException e) {
			throw new FsException("Failed when listing children of directory " + parentDir, e);
		}
	}

	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		return null;
	}

	@Override
	public Object getParent(Object element) {
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		return false;
	}

	public Comparator<Path> getComparator() {
		return comparator;
	}

	public void setComparator(Comparator<Path> comparator) {
		this.comparator = comparator;
	}
}