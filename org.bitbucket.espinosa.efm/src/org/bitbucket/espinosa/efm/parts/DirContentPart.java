package org.bitbucket.espinosa.efm.parts;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;

import org.bitbucket.espinosa.efm.comparators.CoreComparator;
import org.bitbucket.espinosa.efm.comparators.FileListComparator;
import org.bitbucket.espinosa.efm.exeptions.UiException;
import org.bitbucket.espinosa.efm.fsutils.DateTimeFormatterProvider;
import org.bitbucket.espinosa.efm.fsutils.FileHandler;
import org.bitbucket.espinosa.efm.fsutils.FileSizeFormatterProvider;
import org.bitbucket.espinosa.efm.fsutils.ParentPath;
import org.bitbucket.espinosa.efm.services.IconProvider;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.layout.TreeColumnLayout;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

/**
 * TreeView based implementation of directory content.
 * Show list of files and directories for the given directory.
 *  
 * @author Espinosa
 */
public class DirContentPart {
	private TreeViewer viewer;	

	private TreeViewerColumn name;
	private TreeViewerColumn size;
	private TreeViewerColumn date;

	private Image fileIcon;
	private Image folderIcon;
	private Image questionIcon;
	private Image arrowUp;
	
	private DirectoryContentProvider contentProvider = new DirectoryContentProvider();
	
	private Comparator<Path> nameColumnCoreComparator = CoreComparator.NAME;

	@Inject
	DateTimeFormatterProvider dtFormatter;

	@Inject
	FileSizeFormatterProvider fszFormatter;

	@Inject
	IconProvider iconProvider;

	@Inject
	EPartService partService;

	@Inject 
	MPart thisPart;
	
	@Inject
	EMenuService menuService;

	@PostConstruct
	public void createControls(Composite parent) {
		fileIcon = iconProvider.createImage("file");
		folderIcon = iconProvider.createImage("folder");
		questionIcon = iconProvider.createImage("questionMarkGray");
		arrowUp = iconProvider.createImage("arrowUp");

		TreeColumnLayout tableColumnLayout = new TreeColumnLayout();
		parent.setLayout(tableColumnLayout);

		viewer = new TreeViewer(parent, 
				SWT.BORDER | 
				SWT.MULTI | 
				SWT.H_SCROLL | 
				SWT.V_SCROLL |
				SWT.FULL_SELECTION
				);
		Tree tree = viewer.getTree();
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tree.setHeaderVisible(true);
		tree.setLinesVisible(true);

		viewer.setContentProvider(contentProvider);
		viewer.setInput(DirectoryContentProvider.getInitialContent());

		TreeViewerColumn fakecol = new TreeViewerColumn(viewer, SWT.NONE);
		fakecol.setLabelProvider(new DoNothingLabelProvider());
		tableColumnLayout.setColumnData(fakecol.getColumn(), new ColumnPixelData(0));

		name = new TreeViewerColumn(viewer, SWT.NONE);
		name.setLabelProvider(new NameLabelProvider());
		name.getColumn().setText("Name");
		name.getColumn().setAlignment(SWT.LEFT);
		tableColumnLayout.setColumnData(name.getColumn(), new ColumnWeightData(100, 50, true));

		size = new TreeViewerColumn(viewer, SWT.NONE);
		size.setLabelProvider(new SizeLabelProvider());
		size.getColumn().setText("Size");
		size.getColumn().setAlignment(SWT.RIGHT);
		tableColumnLayout.setColumnData(size.getColumn(), new ColumnPixelData(100));

		date = new TreeViewerColumn(viewer, SWT.NONE);
		date.setLabelProvider(new DateLabelProvider());
		date.getColumn().setText("Date");
		date.getColumn().setAlignment(SWT.RIGHT);
		tableColumnLayout.setColumnData(date.getColumn(), new ColumnPixelData(100));

		viewer.addDoubleClickListener(event -> {
			// this also works for pressing 'Enter' on directory item
			Path selectedPath = getSelectedItem(event);
			if (selectedPath instanceof ParentPath) {
				openDirectory(((ParentPath)selectedPath).getTarget());
			} 
			else if (Files.isDirectory(selectedPath)) {
				openDirectory(selectedPath);
			}
		});
		
		tree.addListener(SWT.MenuDetect, (e) -> {
			Point ptAbsolute = new Point(e.x, e.y);
			Point pt = tree.toControl(ptAbsolute); // get position relative to the Tree widget	
			if (pt.y < 0)  {  // negative Y means table header
				if (columnUnderPoint(tree, pt) == name.getColumn()) {
					menuService.registerContextMenu(tree, "org.bitbucket.espinosa.efm.part.dircontent.ctxmenu");
					e.doit = true;
				} else {
					e.doit = false;
				}
			} else {
				e.doit = false;
			}
		});

		name.getColumn().addListener(SWT.Selection, (e) -> {
			sortingDirection = computeDirection(name);
			setNameAsSortingColumn();
		});
		size.getColumn().addListener(SWT.Selection, (e) -> {
			sortingDirection = computeDirection(size);
			setSizeAsSortingColumn();
		});
		date.getColumn().addListener(SWT.Selection, (e) -> {
			sortingDirection = computeDirection(date);
			setDateAsSortingColumn();
		});
	}
	
	private TreeColumn columnUnderPoint(Tree tree, Point pt) {
		TreeItem fakeRow = new TreeItem(tree, 0);
		Rectangle rec = fakeRow.getBounds(1);
		fakeRow.dispose();
		if ((pt.x > rec.x)  && (pt.x < (rec.x + rec.width))) {
			return name.getColumn();
		}
		return null;
	}

	public void setNameAsSortingColumn() {
		makeThisSortingColumn(name,
				FileListComparator.get(nameColumnCoreComparator, sortingDirection, dirFirstFlag));
	}
	
	public void setSizeAsSortingColumn() {
		makeThisSortingColumn(size,
				FileListComparator.get(CoreComparator.SIZE, sortingDirection, dirFirstFlag, CoreComparator.NAME, 1));
	}
	
	public void setDateAsSortingColumn() {
		makeThisSortingColumn(date,
				FileListComparator.get(CoreComparator.DATE, sortingDirection, dirFirstFlag));
	}

	/**
	 * Make give column a sorting column, toggle direction if same column is selected repeatedly.
	 */
	private void makeThisSortingColumn(TreeViewerColumn selectedColumn, Comparator<Path> comparator) {
		currentSortingColumn = selectedColumn;
		contentProvider.setComparator(comparator);
		markColumnAsSortedBy(selectedColumn.getColumn(), sortingDirection);
		viewer.refresh();
	}

	private int sortingDirection = 1;
	private TreeViewerColumn currentSortingColumn = name;
	private boolean dirFirstFlag = true;

	private void markColumnAsSortedBy(TreeColumn column, int sortingDirection) {
		column.getParent().setSortColumn(column);
		if (sortingDirection > 0) {
			column.getParent().setSortDirection(SWT.UP);
		} else if (sortingDirection < 0) {
			column.getParent().setSortDirection(SWT.DOWN);
		} else {
			column.getParent().setSortDirection(SWT.NONE);
		}
	}

	private int computeDirection(TreeViewerColumn newSortingColumn) {
		if (newSortingColumn == currentSortingColumn) {
			return sortingDirection * -1; // if column is the same, toggle direction
		} else {
			return 1; // when column is changed, use (start with) ascending direction 
		}
	}

	public TreeViewer getViewer() {
		return viewer;
	}

	private java.nio.file.Path getSelectedItem(DoubleClickEvent event) {
		Path r = null;
		ISelection selection = event.getSelection();
		if (selection instanceof ITreeSelection) {
			r = (Path)((ITreeSelection)selection).getFirstElement();
		} else {
			throw new UiException("Selection is of unknown type " + selection.getClass());
		}
		if (r == null) throw new UiException("Selection returned as null");
		return r;
	}

	public void openDirectory(java.nio.file.Path selectedPath) {
		// change Part label (title)
		if (selectedPath != null) {
			thisPart.setLabel(getFileName(selectedPath));
			thisPart.setTooltip(selectedPath.toString());
		}

		// change content of the main content table
		viewer.setInput(selectedPath);
	}

	private String getFileName(Path path) {
		Path fileNamePathElement = path.getFileName();
		if (fileNamePathElement == null) return path.toString();;
		return path.getFileName().toString();
	}

	/**
	 * React on change in record selection in {@link DirSelectorPart}.
	 * Open selected directory. 
	 */
	@Inject
	public void directorySelectionChanged(
			@Optional @Named(IServiceConstants.ACTIVE_SELECTION) java.nio.file.Path selectedPath) {
		if (selectedPath != null) {
			openDirectory(selectedPath);
		}
	}

	class DoNothingLabelProvider extends StyledCellLabelProvider {
		@Override
		public void update(ViewerCell cell) {
			super.update(cell);
		}
	}

	class NameLabelProvider extends StyledCellLabelProvider {
		@Override
		public void update(ViewerCell cell) {
			Object element = cell.getElement();
			StyledString text = new StyledString();
			Path p = (Path) element;

			if (p instanceof ParentPath) {
				cell.setImage(arrowUp);
				text.append("..");
			}
			else if (Files.isDirectory(p)) {
				cell.setImage(folderIcon);
				text.append(getLabel(p));
			} 
			else if (Files.isRegularFile(p)) {
				cell.setImage(fileIcon);
				text.append(getLabel(p));
			} 
			else {
				cell.setImage(questionIcon);
				text.append(getLabel(p));
			}

			cell.setText(text.toString());
			cell.setStyleRanges(text.getStyleRanges());

			super.update(cell);
		}

		private String getLabel(Path dir) {
			if (dir == null) return "???";
			else if (dir.getFileName() == null) return dir.toString();
			else return dir.getFileName().toString();
		}
	}

	class DateLabelProvider extends StyledCellLabelProvider {
		@Override
		public void update(ViewerCell cell) {
			Object element = cell.getElement();
			StyledString text = new StyledString();
			java.nio.file.Path p = (java.nio.file.Path) element;

			if (p instanceof ParentPath) {
				text.append("..");
			} else if (Files.isDirectory(p)) {
				text.append(FileHandler.getFileLastModifiedTimeAsString(p, dtFormatter.get()));
			} else if (Files.isRegularFile(p)) {
				text.append(FileHandler.getFileLastModifiedTimeAsString(p, dtFormatter.get()));
			} else {
				text.append("");
			}

			cell.setText(text.toString());
			cell.setStyleRanges(text.getStyleRanges());

			super.update(cell);
		}
	}

	class SizeLabelProvider extends StyledCellLabelProvider {
		@Override
		public void update(ViewerCell cell) {
			Object element = cell.getElement();
			StyledString text = new StyledString();
			java.nio.file.Path p = (java.nio.file.Path) element;
			try {
				if (p instanceof ParentPath) {
					text.append("[DIR]");
				} else if (Files.isDirectory(p)) {
					text.append("[DIR]");
				} else if (Files.isRegularFile(p)) {
					text.append(fszFormatter.get().format(FileHandler.getFileSizeAsLong(p)));
				} else {
					text.append("");
				}

				cell.setText(text.toString());
				cell.setStyleRanges(text.getStyleRanges());

				super.update(cell);
			} catch (Exception e) {
				throw new UiException("Failed on " + p.toString(), e);
			}
		}
	}

	@Focus
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	@PreDestroy
	public void dispose() {
	}

	public DateTimeFormatterProvider getDtFormatter() {
		return dtFormatter;
	}

	public void setDtFormatter(DateTimeFormatterProvider dtFormatter) {
		this.dtFormatter = dtFormatter;
	}

	public FileSizeFormatterProvider getFszFormatter() {
		return fszFormatter;
	}

	public void setFszFormatter(FileSizeFormatterProvider fszFormatter) {
		this.fszFormatter = fszFormatter;
	}

	public Comparator<Path> getNameColumnCoreComparator() {
		return nameColumnCoreComparator;
	}

	public void setNameColumnCoreComparator(Comparator<Path> nameColumnCoreComparator) {
		this.nameColumnCoreComparator = nameColumnCoreComparator;
	}
}
