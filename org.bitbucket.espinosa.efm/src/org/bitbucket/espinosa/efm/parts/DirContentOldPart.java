package org.bitbucket.espinosa.efm.parts;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Comparator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;

import org.bitbucket.espinosa.efm.comparators.CompositeComparator;
import org.bitbucket.espinosa.efm.comparators.CoreComparator;
import org.bitbucket.espinosa.efm.exeptions.FsException;
import org.bitbucket.espinosa.efm.fsutils.FileHandler;
import org.bitbucket.espinosa.efm.services.IconProvider;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;

/**
 * Old implementation based on Table/TableView.
 * 
 * I had some visual issues with how focus (active) line is drawn  so instead I decided to give a multi-column TreeView a chance. 
 * 
 * @author Espinosa
 */
public class DirContentOldPart {
	private TableViewer viewer;	

	private TableViewerColumn name;
	private TableViewerColumn size;
	private TableViewerColumn date;
	
	private Image fileIcon;
	private Image folderIcon;
	private Image qustionIcon;
	
	// TODO: make this injectable
	private DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT, FormatStyle.SHORT);
	
	@Inject
	IconProvider iconProvider;
	
	@PostConstruct
	public void createControls(Composite parent) {
		fileIcon = iconProvider.createImage("file");
		folderIcon = iconProvider.createImage("folder");
		qustionIcon = iconProvider.createImage("questionMarkGray");
		
		//parent.setLayout(new GridLayout(1, false));
		TableColumnLayout tableColumnLayout = new TableColumnLayout();
		parent.setLayout(tableColumnLayout);

		viewer = new TableViewer(parent, 
				SWT.BORDER | 
				SWT.MULTI | 
				SWT.H_SCROLL | 
				SWT.V_SCROLL |
				SWT.FULL_SELECTION
				);
		Table table = viewer.getTable();
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		//TableViewerFocusCellManager focusCellManager = new TableViewerFocusCellManager(viewer, new FocusBorderCellHighlighter(v));
		
		name = new TableViewerColumn(viewer, SWT.NONE);
		//name.getColumn().setWidth(100);
		name.setLabelProvider(new NameLabelProvider());
		name.getColumn().setText("Name");
		name.getColumn().setAlignment(SWT.LEFT);
		tableColumnLayout.setColumnData(name.getColumn(), new ColumnWeightData(100, 50, true));

		size = new TableViewerColumn(viewer, SWT.NONE);
		size.setLabelProvider(new SizeLabelProvider());
		size.getColumn().setText("Size");
		size.getColumn().setAlignment(SWT.RIGHT);
		tableColumnLayout.setColumnData(size.getColumn(), new ColumnPixelData(100));
		
		date = new TableViewerColumn(viewer, SWT.NONE);
		date.setLabelProvider(new DateLabelProvider());
		date.getColumn().setText("Date");
		date.getColumn().setAlignment(SWT.RIGHT);
		tableColumnLayout.setColumnData(date.getColumn(), new ColumnPixelData(100));
		
		viewer.setContentProvider(new DirectoryContentProvider());
		viewer.setInput(DirectoryContentProvider.getInitialContent());
	}
	
	/**
	 * React on change in record selection in {@link DirSelectorPart}.
	 * Open selected directory. 
	 */
	@Inject
	public void directorySelectionChanged(
			@Optional @Named(IServiceConstants.ACTIVE_SELECTION) java.nio.file.Path selectedPath) {
		if (selectedPath != null) {
			//viewer.setInput(((IStructuredContentProvider)viewer.getContentProvider()).getElements(selectedPath));
			viewer.setInput(selectedPath);
		}
	}
	
	class NameLabelProvider extends StyledCellLabelProvider {
		@Override
		public void update(ViewerCell cell) {
			Object element = cell.getElement();
			StyledString text = new StyledString();
			java.nio.file.Path p = (java.nio.file.Path) element;
			text.append(getLabel(p));
			
			if (Files.isDirectory(p)) {
				cell.setImage(folderIcon);
			} else if (Files.isRegularFile(p)) {
				cell.setImage(fileIcon);
			} else {
				cell.setImage(qustionIcon);
			}
			
			cell.setText(text.toString());
			cell.setStyleRanges(text.getStyleRanges());
			
			super.update(cell);
		}

		private String getLabel(java.nio.file.Path dir) {
			if (dir == null) return "???";
			else if (dir.getFileName() == null) return dir.toString();
			else return dir.getFileName().toString();
		}
	}
	
	class DateLabelProvider extends StyledCellLabelProvider {
		@Override
		public void update(ViewerCell cell) {
			Object element = cell.getElement();
			StyledString text = new StyledString();
			java.nio.file.Path p = (java.nio.file.Path) element;
			
			if (Files.isDirectory(p)) {
				text.append(FileHandler.getFileLastModifiedTimeAsString(p, formatter));
			} else if (Files.isRegularFile(p)) {
				text.append(FileHandler.getFileLastModifiedTimeAsString(p, formatter));
			} else {
				text.append("");
			}
			
			cell.setText(text.toString());
			cell.setStyleRanges(text.getStyleRanges());

			super.update(cell);
		}
	}
	
	class SizeLabelProvider extends StyledCellLabelProvider {
		@Override
		public void update(ViewerCell cell) {
			Object element = cell.getElement();
			StyledString text = new StyledString();
			java.nio.file.Path p = (java.nio.file.Path) element;
			
			if (Files.isDirectory(p)) {
				text.append("[DIR]");
			} else if (Files.isRegularFile(p)) {
				text.append(FileHandler.getFileSizeAsString(p));
			} else {
				text.append("");
			}
			
			cell.setText(text.toString());
			cell.setStyleRanges(text.getStyleRanges());

			super.update(cell);
		}
	}
	
	static class DirectoryContentProvider implements IStructuredContentProvider {
		
		private static final Comparator<Path> directoryFirstThenNames = new CompositeComparator<Path>(
				CoreComparator.DIRECTORY_FIRST,
				CoreComparator.NAME
				);
		
		private Comparator<Path> comparator = directoryFirstThenNames;

		public static java.nio.file.Path[] getInitialContent() {
			return null;
		}

		@Override
		public Object[] getElements(Object parentElement) {
			java.nio.file.Path parentDir = (java.nio.file.Path) parentElement;
			if (!Files.isReadable(parentDir)) {
				return new java.nio.file.Path[]{};
			}
			try {
				 java.nio.file.Path[] r = Files.list(parentDir).sorted(comparator).toArray(size -> new java.nio.file.Path[size]);
				 return r;
			} catch (IOException e) {
				throw new FsException("Failed when listing children of directory " + parentDir, e);
			}
		}

		@Override
		public void dispose() {
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}
	
	@Focus
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	@PreDestroy
	public void dispose() {
		folderIcon.dispose();
	}
}
