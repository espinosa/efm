package org.bitbucket.espinosa.efm.parts;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.bitbucket.espinosa.efm.exeptions.FsException;
import org.bitbucket.espinosa.efm.services.IconProvider;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;


public class DirSelectorPart {
	private TreeViewer viewer;
	private Image folderIcon;
	private Image openFolderIcon;
	
	private Object openedPath;
	
	@Inject
	ESelectionService service;
	
	@Inject
	IconProvider iconProvider;

	@PostConstruct
	public void createControls(Composite parent) {
		folderIcon = iconProvider.createImage("folder");
		openFolderIcon = iconProvider.createImage("folderOpen");
		
		viewer = new TreeViewer(parent, SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setInput(ViewContentProvider.getInitialContent());
		
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
				Object oldOpenedPath = openedPath;
				openedPath = selection.getFirstElement();
				service.setSelection(openedPath);
				
				viewer.refresh(oldOpenedPath);
				viewer.refresh(openedPath);
			}
		});
	}

	static class ViewContentProvider implements ITreeContentProvider {

		public static java.nio.file.Path[] getInitialContent() {
			List<java.nio.file.Path> r = new ArrayList<>();
			Iterable<java.nio.file.Path> fsRoots = FileSystems.getDefault().getRootDirectories();
			for (java.nio.file.Path fsRoot : fsRoots) {
				r.add(fsRoot);
			}
			return r.toArray(new java.nio.file.Path[]{});
		}

		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}

		@Override
		public void dispose() {
		}

		@Override
		public Object[] getElements(Object inputElement) {
			return (java.nio.file.Path[]) inputElement;
		}

		/**
		 * List only directories
		 */
		@Override
		public Object[] getChildren(Object parentElement) {
			java.nio.file.Path parentDir = (java.nio.file.Path) parentElement;
			if (!Files.isReadable(parentDir)) {
				return new java.nio.file.Path[]{};
			}
			try {
				 java.nio.file.Path[] r = Files.list(parentDir).filter(Files::isDirectory).toArray(size -> new java.nio.file.Path[size]);
				 return r;
			} catch (IOException e) {
				throw new FsException("Failed when listing children of directory " + parentDir, e);
			}
		}

		@Override
		public Object getParent(Object element) {
			File file = (File) element;
			return file.getParentFile();
		}

		@Override
		public boolean hasChildren(Object element) {
			java.nio.file.Path path = (java.nio.file.Path) element;
			if (Files.isDirectory(path)) {
				return true;
			}
			return false;
		}
	}

	class ViewLabelProvider extends StyledCellLabelProvider {
		@Override
		public void update(ViewerCell cell) {
			Object element = cell.getElement();
			StyledString text = new StyledString();
			java.nio.file.Path dir = (java.nio.file.Path) element;

			text.append(getLabel(dir));
			
			if (dir.equals(getSelectedPath())) {
				cell.setImage(openFolderIcon);
			} else {
				cell.setImage(folderIcon);
			}
			
			cell.setText(text.toString());
			cell.setStyleRanges(text.getStyleRanges());

			super.update(cell);
		}

		private String getLabel(java.nio.file.Path dir) {
			if (dir == null) return "???";
			else if (dir.getFileName() == null) return dir.toString();
			else return dir.getFileName().toString();
		}
		
		private java.nio.file.Path getSelectedPath() {
			ISelection selection = viewer.getSelection();
			if (selection instanceof IStructuredSelection) {
				IStructuredSelection structured = (IStructuredSelection) selection;
				if (structured.getFirstElement() != null) {
					return ((java.nio.file.Path)structured.getFirstElement());
				}
			}
			return null;
		}
	}

	@Focus
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	@PreDestroy
	public void dispose() {
		folderIcon.dispose();
	}
}
