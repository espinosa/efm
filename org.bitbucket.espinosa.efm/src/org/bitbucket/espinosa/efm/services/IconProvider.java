package org.bitbucket.espinosa.efm.services;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

@Creatable
public class IconProvider {
	
	public Image createImage(String imageName) {
		Bundle bundle = FrameworkUtil.getBundle(IconProvider.class);
		URL url = FileLocator.find(bundle, new Path("icons/" + imageName + ".png"), null);
		ImageDescriptor imageDcr = ImageDescriptor.createFromURL(url);
		return imageDcr.createImage();
	}
}
