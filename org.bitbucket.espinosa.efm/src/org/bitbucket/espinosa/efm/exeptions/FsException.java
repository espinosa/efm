package org.bitbucket.espinosa.efm.exeptions;

public class FsException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public FsException() {
		super();
	}

	public FsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public FsException(String message, Throwable cause) {
		super(message, cause);
	}

	public FsException(String message) {
		super(message);
	}

	public FsException(Throwable cause) {
		super(cause);
	}
}
