package org.bitbucket.espinosa.efm.exeptions;

public class UiException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public UiException() {
		super();
	}

	public UiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UiException(String message, Throwable cause) {
		super(message, cause);
	}

	public UiException(String message) {
		super(message);
	}

	public UiException(Throwable cause) {
		super(cause);
	}
}
