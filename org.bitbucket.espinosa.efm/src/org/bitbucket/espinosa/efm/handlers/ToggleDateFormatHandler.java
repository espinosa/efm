package org.bitbucket.espinosa.efm.handlers;

import javax.inject.Inject;
import javax.inject.Named;

import org.bitbucket.espinosa.efm.fsutils.CompactDateTimeFormatterProvider;
import org.bitbucket.espinosa.efm.fsutils.LongDateTimeFormatterProvider;
import org.bitbucket.espinosa.efm.parts.DirContentPart;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

public class ToggleDateFormatHandler {
	
	@Inject
	CompactDateTimeFormatterProvider compactDTF;
	
	@Inject
	LongDateTimeFormatterProvider longDTF;
	
	@Execute
	public void execute(MPart mPart, @Optional @Named("org.bitbucket.espinosa.efm.menu.toggleDateFormat.parameter") String param) {
		DirContentPart part = (DirContentPart)mPart.getObject();
		
		if (param == null) param = "compact";
		if (param.equals("compact")) {
			part.setDtFormatter(compactDTF);
		} else if (param.equals("long")) {
			part.setDtFormatter(longDTF);
		} else {
			part.setDtFormatter(compactDTF);
		}
		
		part.getViewer().refresh();
		part.getViewer().getTree().getColumn(3).pack(); // auto resize column to match longer/shorter format change; pack() has to be called after refresh() 
	}
}
