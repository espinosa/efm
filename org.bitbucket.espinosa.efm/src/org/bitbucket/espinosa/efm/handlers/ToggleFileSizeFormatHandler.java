package org.bitbucket.espinosa.efm.handlers;

import javax.inject.Inject;
import javax.inject.Named;

import org.bitbucket.espinosa.efm.fsutils.FileSizeFormatterProvider;
import org.bitbucket.espinosa.efm.parts.DirContentPart;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

public class ToggleFileSizeFormatHandler {
	
	@Inject
	FileSizeFormatterProvider.InFull inFull;
	
	@Inject
	FileSizeFormatterProvider.InBinaryPrefix inBinaryPrefix;
	
	@Execute
	public void execute(MPart mPart, @Optional @Named("org.bitbucket.espinosa.efm.menu.toggleFileSizeFormat.parameter") String param) {
		DirContentPart part = (DirContentPart)mPart.getObject();
		
		if (param == null) param = "full";
		if (param.equals("full")) {
			part.setFszFormatter(inFull);
		} else if (param.equals("prefixed")) {
			part.setFszFormatter(inBinaryPrefix);
		} else {
			part.setFszFormatter(inFull);
		}
		
		part.getViewer().refresh();
		part.getViewer().getTree().getColumn(2).pack(); // auto resize column to match longer/shorter format change; pack() has to be called after refresh() 
	}
}
