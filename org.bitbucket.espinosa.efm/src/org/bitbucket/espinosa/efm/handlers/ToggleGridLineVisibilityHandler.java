package org.bitbucket.espinosa.efm.handlers;

import org.bitbucket.espinosa.efm.parts.DirContentPart;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.widgets.Tree;

public class ToggleGridLineVisibilityHandler {
	
	@Execute
	public void execute(MPart mPart) {
		DirContentPart part = (DirContentPart)mPart.getObject();
		Tree tree = part.getViewer().getTree();
		tree.setLinesVisible(!tree.getLinesVisible());
	}
}
