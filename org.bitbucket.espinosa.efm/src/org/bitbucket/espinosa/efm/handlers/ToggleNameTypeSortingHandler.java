package org.bitbucket.espinosa.efm.handlers;

import javax.inject.Named;

import org.bitbucket.espinosa.efm.comparators.CoreComparator;
import org.bitbucket.espinosa.efm.parts.DirContentPart;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

public class ToggleNameTypeSortingHandler {
	
	@Execute
	public void execute(
			MPart mPart, 
			@Optional @Named("org.bitbucket.espinosa.efm.parameter.toggleNameTypeSorting") String param
			) {
		DirContentPart part = (DirContentPart)mPart.getObject();
		
		if (param == null) param = "name";
		if (param.equals("name")) {
			part.setNameColumnCoreComparator(CoreComparator.NAME);
		} else if (param.equals("type")) {
			part.setNameColumnCoreComparator(CoreComparator.TYPE_NAME);
		} else {
			part.setNameColumnCoreComparator(CoreComparator.NAME);
		}
		
		part.setNameAsSortingColumn();
		part.getViewer().refresh(); 
	}
}
