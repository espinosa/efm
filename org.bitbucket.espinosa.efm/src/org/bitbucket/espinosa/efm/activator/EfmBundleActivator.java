package org.bitbucket.espinosa.efm.activator;

import org.bitbucket.espinosa.efm.fsutils.CompactDateTimeFormatterProvider;
import org.bitbucket.espinosa.efm.fsutils.DateTimeFormatterProvider;
import org.bitbucket.espinosa.efm.fsutils.FileSizeFormatterProvider;
import org.eclipse.e4.core.di.InjectorFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

@SuppressWarnings("restriction")
public class EfmBundleActivator implements BundleActivator {

	/**
	 * See
	 * http://stackoverflow.com/questions/23769212/use-interface-with-di-and-creatable-in-e4-application
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		InjectorFactory.getDefault()
		.addBinding(DateTimeFormatterProvider.class)
		.implementedBy(CompactDateTimeFormatterProvider.class);
		
		InjectorFactory.getDefault()
		.addBinding(FileSizeFormatterProvider.class)
		.implementedBy(FileSizeFormatterProvider.InFull.class);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
	}
}
