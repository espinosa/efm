package org.bitbucket.espinosa.efm.comparator;

import static org.junit.Assert.assertEquals;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.bitbucket.espinosa.efm.comparators.CompositeComparator;
import org.bitbucket.espinosa.efm.comparators.FileListComparator;
import org.bitbucket.espinosa.efm.comparators.CoreComparator;
import org.bitbucket.espinosa.efm.dummyfs.PathRenderer;
import org.bitbucket.espinosa.efm.dummyfs.TestDirPath;
import org.bitbucket.espinosa.efm.dummyfs.TestFilePath;
import org.junit.Before;
import org.junit.Test;

/**
 * Test {@link CoreComparator} and {@link CompositeComparator}
 * 
 * @author Espinosa
 */
public class FileListComparatorTest {

	List<Path> sampleFiles = Arrays.asList(new Path[]{
			new TestDirPath("DirA", "2016-06-12 21:48"),
			new TestDirPath("DirB", "2016-08-10 01:37"),
			new TestDirPath("DirC", "2015-12-06 23:37"),
			new TestFilePath("aaa",         4_713L, "2017-09-10 14:05"),
			new TestFilePath("Bbb", 4_384_096_256L, "2014-01-09 12:15"),
			new TestFilePath("bbb",   113_110_496L, "2011-03-01 01:45"),
			new TestFilePath("ccc", 1_531_445_248L, "2002-12-08 06:55"),
			new TestFilePath("Zzz",         1_129L, "2017-08-03 22:05"),
			new TestFilePath("zzz",        27_969L, "2015-08-12 06:07"),
	});

	@Before
	public void setUp() throws Exception {
		Collections.shuffle(sampleFiles);
	}

	@Test
	public void compositeByName() {
		Comparator<Path> comparator = FileListComparator.get(CoreComparator.NAME, 1, true);
		List<Path> r = sampleFiles.stream().sorted(comparator).collect(Collectors.toList());
		
		assertEquals("" +
				"      DirA,               0,  2016-06-12T21:48\n" + 
				"      DirB,               0,  2016-08-10T01:37\n" + 
				"      DirC,               0,  2015-12-06T23:37\n" + 
				"       Bbb,      4384096256,  2014-01-09T12:15\n" + 
				"       Zzz,            1129,  2017-08-03T22:05\n" + 
				"       aaa,            4713,  2017-09-10T14:05\n" + 
				"       bbb,       113110496,  2011-03-01T01:45\n" + 
				"       ccc,      1531445248,  2002-12-08T06:55\n" + 
				"       zzz,           27969,  2015-08-12T06:07",
				PathRenderer.toMultiline(r));
	}
	
	@Test
	public void compositeBySize() {
		Comparator<Path> comparator = FileListComparator.get(CoreComparator.SIZE, 1, true);
		List<Path> r = sampleFiles.stream().sorted(comparator).collect(Collectors.toList());
		
		assertEquals("" +
				"      DirA,               0,  2016-06-12T21:48\n" + // because directories have no meaningful size they are sorted by name
				"      DirB,               0,  2016-08-10T01:37\n" + 
				"      DirC,               0,  2015-12-06T23:37\n" + 
				"       Zzz,            1129,  2017-08-03T22:05\n" + // only regular files are sorted by file size
				"       aaa,            4713,  2017-09-10T14:05\n" + 
				"       zzz,           27969,  2015-08-12T06:07\n" + 
				"       bbb,       113110496,  2011-03-01T01:45\n" + 
				"       ccc,      1531445248,  2002-12-08T06:55\n" + 
				"       Bbb,      4384096256,  2014-01-09T12:15",
				PathRenderer.toMultiline(r));
	}
	
	@Test
	public void compositeByModifiedDate() {
		Comparator<Path> comparator = FileListComparator.get(CoreComparator.DATE, 1, true);
		List<Path> r = sampleFiles.stream().sorted(comparator).collect(Collectors.toList());
		
		assertEquals("" +
				"      DirC,               0,  2015-12-06T23:37\n" + 
				"      DirA,               0,  2016-06-12T21:48\n" + 
				"      DirB,               0,  2016-08-10T01:37\n" + 
				"       ccc,      1531445248,  2002-12-08T06:55\n" + 
				"       bbb,       113110496,  2011-03-01T01:45\n" + 
				"       Bbb,      4384096256,  2014-01-09T12:15\n" + 
				"       zzz,           27969,  2015-08-12T06:07\n" + 
				"       Zzz,            1129,  2017-08-03T22:05\n" + 
				"       aaa,            4713,  2017-09-10T14:05",
				PathRenderer.toMultiline(r));
	}
	
	// --- descending
	
	@Test
	public void compositeByName_desc() {
		Comparator<Path> comparator = FileListComparator.get(CoreComparator.NAME, -1, true);
		List<Path> r = sampleFiles.stream().sorted(comparator).collect(Collectors.toList());
		
		assertEquals("" +
				"      DirC,               0,  2015-12-06T23:37\n" + 
				"      DirB,               0,  2016-08-10T01:37\n" + 
				"      DirA,               0,  2016-06-12T21:48\n" + 
				"       zzz,           27969,  2015-08-12T06:07\n" + 
				"       ccc,      1531445248,  2002-12-08T06:55\n" + 
				"       bbb,       113110496,  2011-03-01T01:45\n" + 
				"       aaa,            4713,  2017-09-10T14:05\n" + 
				"       Zzz,            1129,  2017-08-03T22:05\n" + 
				"       Bbb,      4384096256,  2014-01-09T12:15",
				PathRenderer.toMultiline(r));
	}
	
	@Test
	public void compositeBySize_desc() {
		Comparator<Path> comparator = FileListComparator.get(CoreComparator.SIZE, -1, true, CoreComparator.NAME, 1);
		List<Path> r = sampleFiles.stream().sorted(comparator).collect(Collectors.toList());
		
		assertEquals("" +
				"      DirA,               0,  2016-06-12T21:48\n" + 
				"      DirB,               0,  2016-08-10T01:37\n" + 
				"      DirC,               0,  2015-12-06T23:37\n" + 
				"       Bbb,      4384096256,  2014-01-09T12:15\n" + 
				"       ccc,      1531445248,  2002-12-08T06:55\n" + 
				"       bbb,       113110496,  2011-03-01T01:45\n" + 
				"       zzz,           27969,  2015-08-12T06:07\n" + 
				"       aaa,            4713,  2017-09-10T14:05\n" + 
				"       Zzz,            1129,  2017-08-03T22:05",
				PathRenderer.toMultiline(r));
	}
	
	@Test
	public void compositeByModifiedDate_desc() {
		Comparator<Path> comparator = FileListComparator.get(CoreComparator.DATE, -1, true);
		List<Path> r = sampleFiles.stream().sorted(comparator).collect(Collectors.toList());
		
		assertEquals("" +
				"      DirB,               0,  2016-08-10T01:37\n" + 
				"      DirA,               0,  2016-06-12T21:48\n" + 
				"      DirC,               0,  2015-12-06T23:37\n" + 
				"       aaa,            4713,  2017-09-10T14:05\n" + 
				"       Zzz,            1129,  2017-08-03T22:05\n" + 
				"       zzz,           27969,  2015-08-12T06:07\n" + 
				"       Bbb,      4384096256,  2014-01-09T12:15\n" + 
				"       bbb,       113110496,  2011-03-01T01:45\n" + 
				"       ccc,      1531445248,  2002-12-08T06:55",
				PathRenderer.toMultiline(r));
	}
}
