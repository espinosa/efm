package org.bitbucket.espinosa.efm.dummyfs;

public class TestFilePath extends DummyPath {
	
	public TestFilePath(String fileNameAsString, long size, String modified) {
		super(fileNameAsString, size, modified);
	}
	
	public TestFilePath(String fileNameAsString) {
		super(fileNameAsString, 0, null);
	}
	
	@Override
	public boolean isDirectory() {
		return false;
	}

	@Override
	public boolean isRegularFile() {
		return true;
	}
}
