package org.bitbucket.espinosa.efm.dummyfs;

import java.io.IOException;
import java.nio.file.FileStore;
import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.WatchService;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.nio.file.spi.FileSystemProvider;
import java.util.Set;

import org.bitbucket.espinosa.efm.util.NotImplementedException;

public class TestFileSystem extends FileSystem {
	public static final TestFileSystem INSTANCE = new TestFileSystem(); 

	@Override
	public FileSystemProvider provider() {
		return TestFileSystemProvider.INSTANCE;
	}
	
	// ----------

	@Override
	public void close() throws IOException {
		throw new NotImplementedException();
	}

	@Override
	public boolean isOpen() {
		throw new NotImplementedException();
	}

	@Override
	public boolean isReadOnly() {
		throw new NotImplementedException();
	}

	@Override
	public String getSeparator() {
		throw new NotImplementedException();
	}

	@Override
	public Iterable<Path> getRootDirectories() {
		throw new NotImplementedException();
	}

	@Override
	public Iterable<FileStore> getFileStores() {
		throw new NotImplementedException();
	}

	@Override
	public Set<String> supportedFileAttributeViews() {
		throw new NotImplementedException();
	}

	@Override
	public Path getPath(String first, String... more) {
		throw new NotImplementedException();
	}

	@Override
	public PathMatcher getPathMatcher(String syntaxAndPattern) {
		throw new NotImplementedException();
	}

	@Override
	public UserPrincipalLookupService getUserPrincipalLookupService() {
		throw new NotImplementedException();
	}

	@Override
	public WatchService newWatchService() throws IOException {
		throw new NotImplementedException();
	}

}
