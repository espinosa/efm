package org.bitbucket.espinosa.efm.dummyfs;

public class TestDirPath extends DummyPath {
	public TestDirPath(String fileNameAsString, String modified) {
		super(fileNameAsString, 0, modified);
	}
	
	@Override
	public boolean isDirectory() {
		return true;
	}

	@Override
	public boolean isRegularFile() {
		return false;
	}
}
