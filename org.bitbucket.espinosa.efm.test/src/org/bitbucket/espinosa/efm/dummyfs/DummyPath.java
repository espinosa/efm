package org.bitbucket.espinosa.efm.dummyfs;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchEvent.Modifier;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

import org.bitbucket.espinosa.efm.util.NotImplementedException;

public abstract class DummyPath implements Path {
	private final String fileNameAsString;
	private final long size;
	private final LocalDateTime modified;
	
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
	
	public DummyPath(String fileNameAsString, long size, String modified) {
		this.fileNameAsString = fileNameAsString;
		this.size = size;
		if (modified != null) 
			this.modified = LocalDateTime.parse(modified, formatter);
		else 
			this.modified = null;
	}
	
	@Override
	public String toString() {
		return fileNameAsString;
	}
	
	public String getFileNameAsString() {
		return fileNameAsString;
	}
	
	public long getSize() {
		return size;
	}

	public LocalDateTime getModified() {
		return modified;
	}
	
	public abstract boolean isDirectory();
	
	public abstract boolean isRegularFile();
	
	@Override
	public int compareTo(Path other) {
		// this is normally implemented on FS OS provider level, 
		// like sun.nio.fs.WindowsPath
		// so it reflects given OS sorting preferences, like ignoring case on Windows.
		return this.toString().compareTo(other.toString());
	}
	
	// ------------------------------
	
	@Override
	public FileSystem getFileSystem() {
		return TestFileSystem.INSTANCE;
	}

	@Override
	public Path getFileName() {
		return this;
	}
	
	@Override
	public boolean isAbsolute() {
		return true;
	}
	
	// ----------------------------------------

	@Override
	public Path getRoot() {
		throw new NotImplementedException();
	}

	@Override
	public Path getParent() {
		throw new NotImplementedException();
	}

	@Override
	public int getNameCount() {
		return 1;
	}

	@Override
	public Path getName(int index) {
		throw new NotImplementedException();
	}

	@Override
	public Path subpath(int beginIndex, int endIndex) {
		throw new NotImplementedException();
	}

	@Override
	public boolean startsWith(Path other) {
		throw new NotImplementedException();
	}

	@Override
	public boolean startsWith(String other) {
		throw new NotImplementedException();
	}

	@Override
	public boolean endsWith(Path other) {
		throw new NotImplementedException();
	}

	@Override
	public boolean endsWith(String other) {
		throw new NotImplementedException();
	}

	@Override
	public Path normalize() {
		throw new NotImplementedException();
	}

	@Override
	public Path resolve(Path other) {
		throw new NotImplementedException();
	}

	@Override
	public Path resolve(String other) {
		throw new NotImplementedException();
	}

	@Override
	public Path resolveSibling(Path other) {
		throw new NotImplementedException();
	}

	@Override
	public Path resolveSibling(String other) {
		throw new NotImplementedException();
	}

	@Override
	public Path relativize(Path other) {
		throw new NotImplementedException();
	}

	@Override
	public URI toUri() {
		throw new NotImplementedException();
	}

	@Override
	public Path toAbsolutePath() {
		throw new NotImplementedException();
	}

	@Override
	public Path toRealPath(LinkOption... options) throws IOException {
		throw new NotImplementedException();
	}

	@Override
	public File toFile() {
		throw new NotImplementedException();
	}

	@Override
	public WatchKey register(WatchService watcher, Kind<?>[] events, Modifier... modifiers) throws IOException {
		throw new NotImplementedException();
	}

	@Override
	public WatchKey register(WatchService watcher, Kind<?>... events) throws IOException {
		throw new NotImplementedException();
	}

	@Override
	public Iterator<Path> iterator() {
		throw new NotImplementedException();
	}
}
