package org.bitbucket.espinosa.efm.dummyfs;

import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.ZoneOffset;

import org.bitbucket.espinosa.efm.util.NotImplementedException;

public class DummyBasicFileAttributes implements BasicFileAttributes {
	private static final ZoneOffset ZONE_OFFSET = ZoneOffset.ofTotalSeconds(0);
	private final DummyPath path;

	public DummyBasicFileAttributes(Path path, Class<? extends BasicFileAttributes> type, LinkOption... options) {
		this.path = (DummyPath)path;
	}

	@Override
	public FileTime lastModifiedTime() {
		return FileTime.fromMillis(
				path.getModified().toInstant(ZONE_OFFSET).toEpochMilli());
	}
	
	@Override
	public boolean isRegularFile() {
		return path.isRegularFile();
	}
	
	@Override
	public boolean isDirectory() {
		return path.isDirectory();
	}
	
	@Override
	public long size() {
		return path.getSize();
	}
	
	@Override
	public boolean isOther() {
		return false;
	}
	
	// -------

	@Override
	public FileTime lastAccessTime() {
		throw new NotImplementedException();
	}

	@Override
	public FileTime creationTime() {
		throw new NotImplementedException();
	}

	@Override
	public boolean isSymbolicLink() {
		throw new NotImplementedException();
	}

	@Override
	public Object fileKey() {
		throw new NotImplementedException();
	}
}
