package org.bitbucket.espinosa.efm.dummyfs;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class PathRenderer {

	public static String toMultiline(List<Path> paths) {
		return paths.stream()
				.map(p -> (DummyPath)p)
				.map(PathRenderer::dummyPathToString)
				.collect(Collectors.joining("\n"));
	}

	public static String dummyPathToString(DummyPath p) {
		return String.format("%10s, %15s, %17s", 
				p.getFileNameAsString(),
				p.getSize(),
				p.getModified());
	}
}
