package org.bitbucket.espinosa.efm.fsutils;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.bitbucket.espinosa.efm.dummyfs.TestFilePath;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TestFileHandler {
    private String path; 
    private String expectedResult;
    
    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {     
                 { "example.doc",   "doc"}, 
                 { "app.exe",       "exe"},
                 { "bindata",        ""},
                 { ".settings",      ""},
                 { "oddname.",       ""}
           });
    }

	public TestFileHandler(String path, String expectedResult) {
		this.path = path;
		this.expectedResult = expectedResult;
	}
	
	@Test 
	public void execute() {
		String actual = FileHandler.getFileType(new TestFilePath(path));
		assertEquals(expectedResult, actual);
	}
}
